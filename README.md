# Description
An intuitive and fast solution that allows to use EmojiOne codes for unicode
character as shortcut in any window to insert emojis easily.👍😃  
You also can restrict the shortcuts for only using it in some programms.😎

# Use case
You are typing a message in WhatsApp Web and want to insert quickly an emoji.
You can write `I love you :heart:` and it will output `I love you ❤`
It's much faster than taking the mouse, clicking the emojis icon and then
choosing your favorite icon.  
In fact what it really does it's inserting the unicode of an heart (U-2764) that
will be displayed in a slightly different way depending of your application ❤. 

# Coding logic
All EmojiOne codes have the syntaxe `:code:` but we can't have an Hotstring
ending by a colon. So each Hotstring is build without the last colon (`:code`).
Then *emojiOne()* function will read the ending character (*A_EndChar*) to know 
if user typed the entire EmojiOne code (`:code:`) or something else (`:code.`
for example). In this last case, Hotstring won't be replaced.

# Credits
This script use attributes from EmojiOne (http://emojione.com) to build all
the Hotstrings. Attributes are more specifically availible in their GitHub
repository :
https://github.com/emojione/emojione/blob/master/extras/alpha-codes/eac.csv