/*
  ==============================================================================
  EmojiOne
  by D7ego
  More info at https://gitlab.com/smart-autohotkey/emojiOne
  ==============================================================================


  ======= DESCRIPTION ==========================================================
  An intuitive and fast solution that allows to use EmojiOne codes for unicode
  character as shortcut in any window to insert emojis easily. You also can
  restrict the shortcuts for only using it in some programms.


  ======= USE CASE =============================================================
  You are typing a message in WhatsApp Web and want to insert quickly an emoji.
  You can write "I love you :heart:" and it will output "I love you ❤".
  It's much faster than taking the mouse, clicking the emojis icon and then
  choosing your favorite icon.


  ======= CODING LOGIC =========================================================
  All EmojiOne codes have the syntaxe :code: but we can't have an Hotstring
  ending by a colon. So each Hotstring is build without the last colon (:code).
  Then function emojiOne() will read the ending character (A_EndChar) to know if
  user typed the entire EmojiOne code (:code:) or something else (:code. for
  example). In this last case, Hotstring won't be replaced.


  ======= CREDITS ==============================================================
  This script use attributes from EmojiOne (http://emojione.com) to build all
  the Hotstrings. Attributes are more specifically availible in their GitHub
  repository :
  https://github.com/emojione/emojione/blob/master/extras/alpha-codes/eac.csv
*/


; If EndChar is a colon, insert the unicode emoji.
; Otherwise insert the Hotstring followed by EndChar.
emojiOne(unicode){
  If (A_EndChar = ":") {
    SendInput {U+%unicode%}
  } Else {
    StringMid, s, A_ThisHotKey, 3   ; A_ThisHotKey contains 2 starting colons
    SendRaw %s%%A_EndChar%
  }

  /*
    ; === IfEqual syntaxe ===
    IfEqual, A_EndChar, ":":, SendInput {U+%unicode%}
    Else {
      StringMid, s, A_ThisHotKey, 3
      SendRaw %s%%A_EndChar%
    }
  */
}


SetTitleMatchMode 2   ; Window title of GroupAdd should only contain WinTitle
; Add below a line for each application you want the Hotstrings be activated
GroupAdd, emojiGroup, WhatsChrome
GroupAdd, emojiGroup, Skype
GroupAdd, emojiGroup, Hangouts    ; not tested
GroupAdd, emojiGroup, Messenger   ; not tested
GroupAdd, emojiGroup, Slack       ; not tested
; GroupAdd, emojiGroup, NameOfWindowApplication


; Add "ahk_group emojiGroup" if you want to apply Hotstring to some windows.
; Remove "ahk_group emojiGroup" if you want to apply Hotstrings to all windows.
#IfWinActive
  ; Use of ? to trigger even if it's inside another word
  ; No use of * because we need to evaluate the EndChar
  ; No use of o because we need to print it if it's not a colon
  #Hotstring ?
    ; Include of all Hotstrings if files exists (*i → error-friendly)

    ; Hotstrings from the source alpha codes
    #Include *i emojiOneAlphaCodes.ahk

    ; Hotstrings from the source alias list
    #Include *i emojiOneAliases.ahk

    ; My personalized Hotstrings with EmojiOne syntaxe
    #Include *i emojiOnePersonal.ahk
  #Hotstring ?0
#IfWinActive
