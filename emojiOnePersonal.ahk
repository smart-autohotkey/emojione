; ==============================================================================
; ======= PERSONALIZED HOTSTRINGS ==============================================
; ==============================================================================

:::like::
  emojiOne("1f44d-1f3fb")   ; thumbsup_tone1 👍
return

:::dislike::
  emojiOne("1f44e-1f3fb")   ; :thumbsdown_tone1 👎
return

:::upup::
  emojiOne("261d-1f3fb")    ; point_up_tone1 ☝
return

:::okey::
  emojiOne("1f44c-1f3fb")   ; ok_hand_tone1 👌
return

:::strong::
  emojiOne("1f4aa-1f3fb")   ; muscle_tone1 💪
return

:::fuck::
  emojiOne("1f595-1f3fb")   ; middle_finger_tone1 🖕
return

:::oops::
  emojiOne("1f64a")   ; speak_no_evil 🙊
return

:::lol::
  emojiOne("1f61c")   ; stuck_out_tongue_winking_eye 😜
return

:::haha::
  emojiOne("1f602")   ; joy 😂
return

:::yeah::
  emojiOne("1f61b")   ; stuck_out_tongue 😛
return

:::love::
  emojiOne("1f60d")   ; heart_eyes 😍
return

:::k::
  emojiOne("1f618")   ; ok_hand_tone1 😘
return

:::h::
  emojiOne("1f60e")   ; sunglasses 😎
return
